import {promiseChainInstance} from "../src/";

it('resolve all promises in one instance', () => {
    const promiseChain = promiseChainInstance.get('app');

    const p1 = promiseChain.createPromise();
    const p2 = promiseChain.createPromise();

    promiseChain.resolveAll(10);

    return Promise.all([p1, p2]).then((data) => {
        expect(data).toEqual([10, 10]);
    })
});

it('resolve all promises in several instances', () => {
    const promiseChain1 = promiseChainInstance.get('app1');
    const promiseChain2 = promiseChainInstance.get('app2');

    const p11 = promiseChain1.createPromise();
    const p12 = promiseChain1.createPromise();
    const p21 = promiseChain2.createPromise();
    const p22 = promiseChain2.createPromise();

    promiseChain1.resolveAll(20);
    promiseChain2.resolveAll(30);

    return Promise.all([p11, p12, p21, p22]).then((data) => {
        expect(data).toEqual([20, 20, 30, 30]);
    })
});

