class PromiseChain {
    private resolveList: Array<any>;
    private rejectList: Array<any>;
    private _parametersBag: Array<any>;

    constructor() {
        this.resolveList = [];
        this.rejectList = [];
        this._parametersBag = [];
    }

    createPromise(args: any = {}): Promise<any> {
        this._parametersBag.push(args);

        return new Promise((resolve, reject) => {
            this.resolveList.push(resolve);
            this.rejectList.push(reject);
        });
    }

    resolveAll(args: any) {
        this.resolveList.map((resolve) => resolve(args));
    }

    rejectAll(args: any) {
        this.rejectList.map((reject) => reject(args));
    }

    public get parametersBag(): Array<any> {
        return this._parametersBag;
    }
}

class PromiseChainInstance {
    readonly instances: { [key: string]: PromiseChain; }

    constructor() {
        this.instances = {};
    }

    get(uid: string) {
        if (this.instances.hasOwnProperty(uid)) {
            return this.instances[uid];
        }

        this.instances[uid] = new PromiseChain();

        return this.instances[uid];
    }
}

export const promiseChainInstance = new PromiseChainInstance();
